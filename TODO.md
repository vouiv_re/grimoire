# Todo

* [ ] Faire une recherche
* [ ] Projet damier, avec déplacement d'icone, brouillard de guerre, obstacle, ajout de créature
* [ ] Faire un script pour ajoute(enlever) une propriété à tout les fichies d'une même catégorie
* [ ] Remanier les script pour ne les utiliser qu'une fois
* [ ] Faire un script qui vérifié le formatage des fichiers
* [ ] Faire un script qui génère un nouveau projet
* [ ] Faire un script qui génère une nouvelle catégorie (et qui pemet donc de vérifier que les autres sont cohérente, de les adapter)
* [ ] Infobulle
* [ ] Faire un switch de projet en JS
* [ ] Faire un style par page
* [ ] **6** Changer l'image en fonction du projet
* [ ] **7** Mettre les images dans les datas
* [ ] **8** Faire un générateur de JSON (fiche perso) en bash puis parser les fichiers pour mettre les index et les tags
  * [ ] Faire un script bash qui permet de générer un personnage, lieux, et de tirer des données au hazard !
* [ ] **9** Sur certaine page afficher en plus de la description la liste des tags
* [ ] **10** Gérer les images dans le projet
* [ ] **11** Passer les trois scripts au [shellcheck](https://www.shellcheck.net/)

## Done

* [x] Faire un jeu de donnée test (tlotr)
* [x] **5** Enlever le titre du logo
* [x] **2** Un sytème de tag ? avec une page qui affiche les tags ? Un tableau dans le json
  * [x] Un fichier avec pour chaque tag les pages relatives (il faut vérifier que les entrées sont unique)
  * [x] Modifier le js pour changer chaque élément de la liste en span avec eventListner
  * [x] Faire une page avec tout les tags, l'ajouter en nav
* [x] **1** Améliorer le script pour prendre compte les lieux, les personnage, ...
  * [x] Standardiser tout les fichiers du même type
* [x] Faire un script qui liste le nombre de projet, et qui demande de répondre un chiffre pour savoir lequel on lance, puis lance le index et le tag en fonction stocke cette info dans un .yml
* [x] **3** Faire un script pour lancer les deux scripts bash
* [x] **4** Séparer le code des data !
* [x] Faire un générateur de créateur d'élément, pareil pour créer des listes
* [x] Faire des pages de recap des categories
* [x] Mettre des tags en bas du fichier
* [x] Faire un menu burger, fixé en position
* [x] Au parse générer une page
* [x] Faire un parse/tag/lien
* [x] Faire un script shell qui compte les fichiers et inscrit les noms dans un fichier json
* [ ] ~~Import md > json~~

***

* Fil d'alarme
* Un scénar, une soirée, du fun, de l'action du drame, du mystère dévoilé
* Un damier en octogone
* pour faire un damier avec de la lazure, passer dans un sens avec du scotcht proteger dans l'autre sens et repasser