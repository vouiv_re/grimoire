#!/bin/bash
echo $PWD

cd "$PWD"/src/data || exit

declare -a folders

i=1
for entry in "$PWD"/*; do
    folder="$(basename "$entry" | cut -f1 -d'.')"
    folders+=("$folder")
    ((i++))
done

if [ "${folders[*]}" = "*" ]
then
    echo "Veuillez ajouter un projet dans le répertoire 'src/data'"
    exit
elif [ "${#folders[@]}" -eq 1 ]
then
    folder="0"
    echo "On prend le seul projet que vous avez"
else
    i=1
    echo "Dans quel projet voulez-vous créer une nouvelle entrée ? [2:phuys]"
    for entry in "$PWD"/*; do
        echo "$i) ${folders[i-1]}"
        ((i++))
    done

    read folder

    if [ folder = null ]
    then
        folder="1"
    fi
fi

categories=(artefact character faction place race storyline)

echo
echo "À quelle catégorie cette entrée appartient-elle ? [character]"
i=1
for entry in "${categories[@]}"; do
    echo "$i) ${categories[i-1]}"
    ((i++))
done
read category
if [ -z "$category" ]
then
    category=2
fi
category="${categories[category-1]}"

echo
echo "Quel nom voulez-vous donner à ce fichier ?"
read name

while [ ${#name} -lt 1 ]
do
    echo "Nous avons besoin d'un nom"
    read name
done

newFile="./${folders[folder-1]}/data/$category/${name,,}.json";

echo "{" >> $newFile
echo -e "\t\"name\": \"${name^}\"," >> $newFile

if [ $category = 'character' ]
then
    echo
    echo "Nom de famille :"
    read lastname
    echo -e "\t\"lastname\": \"${lastname^}\"," >> $newFile
    echo
    echo "Classe :"
    read class
    echo -e "\t\"class\": \"${class,,}\"," >> $newFile
    echo
    echo "Genre (h/f) [f] :"
    read gender
    if [ -z "$gender" ]
    then
        gender="f"
    fi
    echo -e "\t\"gender\": \"${gender,,}\"," >> $newFile
    echo
    echo "Race :"
    read race
    echo -e "\t\"race\": \"${race,,}\"," >> $newFile
    echo -e "\t\"description\": \"\"," >> $newFile
    echo
    echo "Age [0] :"
    read age
    if [ -z "$age" ]
    then
        age=0
    fi
    echo -e "\t\"age\": $age," >> $newFile
    echo -e "\t\"power\": []," >> $newFile
    echo -e "\t\"stuff\": []," >> $newFile
    echo -e "\t\"past\": []," >> $newFile
    echo -e "\t\"quest\": []," >> $newFile
    if [ $gender = 'f' ]
    then
        gender='femme'
    else
        gender='homme'
    fi
    echo -e "\t\"tag\": [\"${race,,}\", \"${class,,}\", \"${gender}\"]" >> $newFile
elif [ $category = 'artefact' ]
then
    echo -e "\t\"description\": \"\"," >> $newFile
    echo -e "\t\"power\": []," >> $newFile
    echo -e "\t\"tag\": []" >> $newFile
elif [ $category = 'faction' ]
then
    echo -e "\t\"description\": \"\"," >> $newFile
    echo -e "\t\"tag\": []" >> $newFile
elif [ $category = 'place' ]
then
    echo -e "\t\"description\": \"\"," >> $newFile
    echo -e "\t\"location\": \"\"," >> $newFile
    echo -e "\t\"spot\": []," >> $newFile
    echo -e "\t\"tag\": []" >> $newFile
elif [ $category = 'race' ]
then
    echo -e "\t\"home\": []," >> $newFile
    echo -e "\t\"description\": \"\"," >> $newFile
    echo -e "\t\"tag\": []" >> $newFile
elif [ $category = 'storyline' ]
then
    echo -e "\t\"description\": \"\"," >> $newFile
    echo -e "\t\"step\": []," >> $newFile
    echo -e "\t\"tag\": []" >> $newFile
fi

echo "}" >> $newFile

echo $PWD
cd "./../../" || exit
echo $PWD

bash ./src/script/target.sh